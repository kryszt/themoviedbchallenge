
# Build

Project requires The Movie Database v4 API key to work. 
Token is taken from property `THE_MOVIE_DB_API_KEY_V4`. It can be set either in `gradle.properties` file, or passed as a build parameter:
`./gradlew assembleDebug -PTHE_MOVIE_DB_API_KEY_V4="\"<api_token>"\"`

# Run 

### Default
By default app opens empty list activity.

# Missing features
- UI tests
- GitHub Actions integration 
