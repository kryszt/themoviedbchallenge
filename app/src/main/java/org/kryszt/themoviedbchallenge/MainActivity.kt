package org.kryszt.themoviedbchallenge

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import org.koin.android.viewmodel.ext.android.getViewModel
import org.kryszt.themoviedbchallenge.databinding.ActivityMainBinding
import org.kryszt.themoviedbchallenge.ui.actions.MovieListActionHandler
import org.kryszt.themoviedbchallenge.ui.util.onQuerySubmitted
import org.kryszt.themoviedbchallenge.viewmodel.MovieListViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MovieListViewModel
    private lateinit var binding: ActivityMainBinding

    private val movieListActionsHandler = MovieListActionHandler(this, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        initViewModel()
        initSearchView(binding.searchView)
    }

    private fun initViewModel() {
        viewModel = getViewModel()
        observeViewModel()
    }

    private fun observeViewModel() {
        movieListActionsHandler.observe(viewModel.navigationEvent)
    }

    private fun initSearchView(searchView: SearchView) {
        searchView.onQuerySubmitted {
            searchView.clearFocus()
            viewModel.querySubmitted(it)
        }
    }
}
