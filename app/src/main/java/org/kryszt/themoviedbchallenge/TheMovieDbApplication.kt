package org.kryszt.themoviedbchallenge

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.kryszt.themoviedbchallenge.model.di.modelModule
import org.kryszt.themoviedbchallenge.moviedb.di.networkModule
import org.kryszt.themoviedbchallenge.moviedb.di.useCasesModule
import org.kryszt.themoviedbchallenge.ui.di.uiModule
import org.kryszt.themoviedbchallenge.viewmodel.viewModelModule

class TheMovieDbApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TheMovieDbApplication)
            modules(networkModule, useCasesModule, modelModule, viewModelModule, uiModule)
        }
    }
}
