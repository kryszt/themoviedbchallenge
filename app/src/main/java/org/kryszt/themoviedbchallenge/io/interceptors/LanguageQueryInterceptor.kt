package org.kryszt.themoviedbchallenge.io.interceptors

import java.util.*

class LanguageQueryInterceptor(
    locale: Locale
) : QueryParameterInterceptor("language", locale.toLanguageTag())
