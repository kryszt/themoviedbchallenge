package org.kryszt.themoviedbchallenge.io.interceptors

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Adds parameter to request
 */
open class QueryParameterInterceptor(
    private val parameterName: String,
    private val parameterValue: String
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        return original
            .newBuilder()
            .url(original.url().withExtraParameter(parameterName, parameterValue))
            .build()
            .let { chain.proceed(it) }
    }

    private fun HttpUrl.withExtraParameter(key: String, value: String) =
        newBuilder().addQueryParameter(key, value).build()
}
