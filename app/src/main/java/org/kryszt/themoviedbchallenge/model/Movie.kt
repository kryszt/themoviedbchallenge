package org.kryszt.themoviedbchallenge.model

data class Movie(
    val id: Int,
    val title: String,
    val summary: String,
    val formattedReleaseDate: String,
    val detailsLink: String,
    val pictureUrl: String?
)
