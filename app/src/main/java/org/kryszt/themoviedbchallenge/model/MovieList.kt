package org.kryszt.themoviedbchallenge.model

class MovieList(val movies: List<Movie>, val page: Int, val totalPages: Int) {
    fun addNextList(other: MovieList): MovieList =
        MovieList(
            movies = this.movies + other.movies,
            page = other.page,
            totalPages = other.totalPages
        )

    fun hasMoreToLoad(): Boolean = page < totalPages
}
