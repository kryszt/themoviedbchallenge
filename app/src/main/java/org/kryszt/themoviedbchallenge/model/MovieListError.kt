package org.kryszt.themoviedbchallenge.model

enum class MovieListError {
    NO_QUERY,
    CONNECTION_ERROR
}
