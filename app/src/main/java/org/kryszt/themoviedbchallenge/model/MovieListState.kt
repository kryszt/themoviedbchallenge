package org.kryszt.themoviedbchallenge.model

import org.kryszt.themoviedbchallenge.util.castSafe
import org.kryszt.themoviedbchallenge.util.isOf
import org.kryszt.themoviedbchallenge.util.orElseFalse

sealed class MovieListState {
    object Initial : MovieListState()
    data class Loading(val query: String) : MovieListState()
    data class Error(val query: String?, val error: MovieListError) : MovieListState() {
        constructor(error: MovieListError) : this(null, error)
    }

    data class Ready(
        val query: String,
        val movieList: MovieList,
        val isLoadingMore: Boolean = false,
        val lastError: MovieListError? = null
    ) : MovieListState() {

        fun setLoadingMore(): Ready = copy(isLoadingMore = true)
        fun clearError(): Ready = copy(lastError = null)
        fun withError(error: MovieListError) = copy(lastError = error)
    }

    fun getMovies(): List<Movie> =
        castSafe<Ready>()
            ?.movieList
            ?.movies
            .orEmpty()

    fun getMoviesCount(): Int =
        castSafe<Ready>()
            ?.movieList
            ?.movies
            ?.size
            ?: 0

    fun canLoadMore(): Boolean =
        !isLoading() && hasMoreToLoad()

    fun isEmptyResult(): Boolean =
        castSafe<Ready>()
            ?.movieList
            ?.movies
            ?.isEmpty()
            .orElseFalse()

    /**
     * Checks if data is being loaded (either as initial load or loading more)
     */
    fun isLoading(): Boolean =
        this.isOf<Loading>() || isReadyLoadingMore()

    fun isClearLoading(): Boolean =
        this.isOf<Loading>()

    /**
     * Checks if current state is Ready and is loading more data
     */
    fun isReadyLoadingMore(): Boolean =
        castSafe<Ready>()
            ?.isLoadingMore
            .orElseFalse()

    /**
     * Returns last error from ready state, if available
     */
    fun readyError(): MovieListError? =
        castSafe<Ready>()?.lastError

    private fun hasMoreToLoad(): Boolean =
        castSafe<Ready>()
            ?.movieList
            ?.hasMoreToLoad()
            .orElseFalse()
}


