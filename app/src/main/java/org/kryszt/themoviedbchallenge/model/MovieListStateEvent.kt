package org.kryszt.themoviedbchallenge.model

sealed class MovieListStateEvent {
    object Reload : MovieListStateEvent()
    object LoadMore : MovieListStateEvent()
    class Load(val query: String) : MovieListStateEvent()
    class LoadingError(val error: MovieListError) : MovieListStateEvent()
    class Loaded(val query: String, val movieList: MovieList) : MovieListStateEvent()
}
