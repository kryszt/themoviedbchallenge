package org.kryszt.themoviedbchallenge.model

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import org.kryszt.themoviedbchallenge.util.addTo
import org.kryszt.themoviedbchallenge.util.observeOnUi

class MovieListStateHolder(private val reducer: MovieListStateReducer) : Disposable {

    private val ongoingEvents = CompositeDisposable()

    private val _observableState =
        BehaviorSubject.createDefault<MovieListState>(MovieListState.Initial)

    val currentState: MovieListState
        get() = _observableState.value ?: MovieListState.Initial

    val observableState: Observable<MovieListState> = _observableState.distinctUntilChanged()

    fun reload() {
        handleEvent(MovieListStateEvent.Reload)
    }

    fun loadMore() {
        handleEvent(MovieListStateEvent.LoadMore)
    }

    fun loadList(query: String) {
        handleEvent(MovieListStateEvent.Load(query))
    }

    private fun handleEvent(event: MovieListStateEvent) {
        cancelOngoing()
        reduceEvent(event)
    }

    private fun reduceEvent(event: MovieListStateEvent) {
        val result = reducer.reduceEvent(currentState, event)
        _observableState.onNext(result.nextState)
        result
            .sideEffects
            .observeOnUi()
            .subscribe(this::handleEvent)
            .addTo(ongoingEvents)
    }

    private fun cancelOngoing() {
        ongoingEvents.clear()
    }

    override fun dispose() =
        ongoingEvents.dispose()

    override fun isDisposed(): Boolean =
        ongoingEvents.isDisposed
}
