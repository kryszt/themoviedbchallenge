package org.kryszt.themoviedbchallenge.model

import io.reactivex.Observable
import io.reactivex.Single
import org.kryszt.themoviedbchallenge.model.usecase.SearchMoviesUseCase

class MovieListStateReducer(private val searchMoviesUseCase: SearchMoviesUseCase) {

    fun reduceEvent(currentState: MovieListState, event: MovieListStateEvent): ReducerResult {
        return when (event) {
            is MovieListStateEvent.Load -> reduceToLoadingState(event.query)
            is MovieListStateEvent.LoadingError -> loadError(currentState, event.error)
            is MovieListStateEvent.Loaded -> loaded(currentState, event)
            MovieListStateEvent.LoadMore -> loadMore(currentState)
            MovieListStateEvent.Reload -> reload(currentState)
        }
    }

    private fun loadError(currentState: MovieListState, error: MovieListError): ReducerResult {
        return when (currentState) {
            is MovieListState.Ready -> currentState.withError(error).asResult()
            else -> reduceToError(error)
        }
    }

    private fun loaded(
        currentState: MovieListState,
        loaded: MovieListStateEvent.Loaded
    ): ReducerResult =
        when (currentState) {
            is MovieListState.Ready -> reduceToReady(
                loaded.query,
                currentState.movieList.addNextList(loaded.movieList)
            )
            else -> reduceToReady(loaded.query, loaded.movieList)
        }

    private fun loadMore(currentState: MovieListState): ReducerResult {
        return when (currentState) {
            is MovieListState.Error -> reload(currentState)
            MovieListState.Initial -> reduceToNoQueryError()
            is MovieListState.Loading -> currentState.asResult()
            is MovieListState.Ready -> reduceToLoadingMore(currentState)
        }
    }

    private fun reload(currentState: MovieListState): ReducerResult {
        return when (currentState) {
            is MovieListState.Error -> currentState.query?.let { reduceToLoadingState(it) }
                ?: reduceToNoQueryError()
            MovieListState.Initial -> reduceToNoQueryError()
            is MovieListState.Loading -> reduceToLoadingState(currentState.query)
            is MovieListState.Ready -> reduceToLoadingState(currentState.query)
        }
    }

    private fun reduceToNoQueryError(): ReducerResult =
        reduceToError(MovieListError.NO_QUERY)

    private fun reduceToError(error: MovieListError): ReducerResult =
        MovieListState.Error(error).asResult()

    private fun reduceToReady(query: String, movieList: MovieList): ReducerResult =
        MovieListState.Ready(query, movieList).asResult()

    private fun reduceToLoadingMore(readyState: MovieListState.Ready): ReducerResult =
        readyState
            .setLoadingMore()
            .clearError()
            .asResult(load(readyState.query, readyState.movieList.page + 1))

    private fun reduceToLoadingState(query: String, page: Int = 1): ReducerResult =
        MovieListState.Loading(query).asResult(load(query, page))

    private fun load(query: String, page: Int = 1): Single<MovieListStateEvent> =
        searchMoviesUseCase
            .findMovies(query, page)
            .map<MovieListStateEvent> { MovieListStateEvent.Loaded(query, it) }
            .onErrorReturn { MovieListStateEvent.LoadingError(MovieListError.CONNECTION_ERROR) }
}

class ReducerResult(val nextState: MovieListState, val sideEffects: Observable<MovieListStateEvent>)

fun MovieListState.asResult(sideEffects: Single<MovieListStateEvent>): ReducerResult =
    ReducerResult(this, sideEffects.toObservable())

fun MovieListState.asResult(sideEffects: Observable<MovieListStateEvent> = Observable.empty()): ReducerResult =
    ReducerResult(this, sideEffects)
