package org.kryszt.themoviedbchallenge.model.di

import org.koin.dsl.module
import org.kryszt.themoviedbchallenge.model.MovieListStateHolder
import org.kryszt.themoviedbchallenge.model.MovieListStateReducer
import org.kryszt.themoviedbchallenge.model.usecase.SearchMoviesUseCase

@Suppress("RemoveExplicitTypeArguments")
val modelModule = module {
    factory<MovieListStateReducer> {
        MovieListStateReducer(get<SearchMoviesUseCase>())
    }

    factory<MovieListStateHolder> {
        MovieListStateHolder(get<MovieListStateReducer>())
    }
}
