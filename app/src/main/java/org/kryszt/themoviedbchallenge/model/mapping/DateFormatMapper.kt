package org.kryszt.themoviedbchallenge.model.mapping

interface DateFormatMapper {
    fun mapDate(dateString: String): String?
}
