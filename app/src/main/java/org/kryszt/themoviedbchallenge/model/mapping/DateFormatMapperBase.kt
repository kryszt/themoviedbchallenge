package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.util.tryOrNull
import java.text.SimpleDateFormat
import java.util.*

open class DateFormatMapperBase(
    apiDateFormat: String,
    uiDateFormat: String,
    userLocale: Locale
) : DateFormatMapper {

    private val apiFormatter = SimpleDateFormat(apiDateFormat, userLocale)
    private val uiFormatter = SimpleDateFormat(uiDateFormat, userLocale)

    override fun mapDate(dateString: String): String? =
        dateString
            .let { tryOrNull { apiFormatter.parse(it) } }
            ?.let { tryOrNull { uiFormatter.format(it) } }
}
