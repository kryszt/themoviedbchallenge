package org.kryszt.themoviedbchallenge.model.mapping

object DefaultMovieWebLinkSource : MovieWebLinkSourceBase("https://www.themoviedb.org/movie/")
