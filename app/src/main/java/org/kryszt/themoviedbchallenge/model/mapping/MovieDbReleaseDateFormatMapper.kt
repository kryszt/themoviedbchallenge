package org.kryszt.themoviedbchallenge.model.mapping

import java.util.*

class MovieDbReleaseDateFormatMapper(locale: Locale = Locale.getDefault()) :
    DateFormatMapperBase("yyyy-MM-dd", "MMM d, yyyy", locale)
