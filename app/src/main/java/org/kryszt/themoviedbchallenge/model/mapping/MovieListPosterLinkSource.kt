package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.util.orElse

class MovieListPosterLinkSource(baseUrl: String, imagePixelWidth: Int) :
    PosterLinkSourceBase(baseUrl, selectSizeKey(imagePixelWidth)) {

    companion object {

        private class PosterSize(val widthPx: Int, val sizeKey: String)

        private val posterSizes = listOf(
            PosterSize(45, "w45"),
            PosterSize(92, "w92"),
            PosterSize(154, "w154"),
            PosterSize(185, "w185"),
            PosterSize(300, "w300"),
            PosterSize(500, "w500")
        )

        fun selectSizeKey(widthPx: Int): String =
            posterSizes
                .find { it.widthPx > widthPx }
                .orElse(posterSizes.last())
                .sizeKey
    }
}
