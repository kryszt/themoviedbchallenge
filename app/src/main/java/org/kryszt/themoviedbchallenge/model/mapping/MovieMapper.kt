package org.kryszt.themoviedbchallenge.model.mapping

import androidx.annotation.VisibleForTesting
import org.kryszt.themoviedbchallenge.model.Movie
import org.kryszt.themoviedbchallenge.model.MovieList
import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntry
import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntryList

class MovieMapper(
    private val posterLinkSource: PosterLinkSource = DefaultPosterLinkSource,
    private val movieWebLinkSource: MovieWebLinkSource = DefaultMovieWebLinkSource,
    private val dateFormatter: DateFormatMapper = MovieDbReleaseDateFormatMapper()
) {

    fun mapMovieEntryList(movieEntryList: MovieEntryList): MovieList =
        MovieList(
            movies = movieEntryList.results.map(::mapMovieEntry),
            page = movieEntryList.page,
            totalPages = movieEntryList.totalPages
        )

    @VisibleForTesting
    fun mapMovieEntry(movieEntry: MovieEntry): Movie =
        Movie(
            id = movieEntry.id,
            title = movieEntry.title.orEmpty(),
            summary = movieEntry.overview.orEmpty(),
            detailsLink = movieWebLinkSource.makeMovieWebLink(movieEntry),
            pictureUrl = posterLinkSource.makePosterLink(movieEntry),
            formattedReleaseDate = movieEntry.releaseDate?.let(dateFormatter::mapDate).orEmpty()
        )
}
