package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntry

interface MovieWebLinkSource {
    fun makeMovieWebLink(movie: MovieEntry): String
}
