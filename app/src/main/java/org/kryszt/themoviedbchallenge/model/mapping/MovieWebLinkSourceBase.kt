package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntry

open class MovieWebLinkSourceBase(
    private val baseUrl: String
) : MovieWebLinkSource {

    override fun makeMovieWebLink(movie: MovieEntry): String = baseUrl + movie.id
}
