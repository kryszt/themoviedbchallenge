package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntry

interface PosterLinkSource {
    fun makePosterLink(movie: MovieEntry): String?
}
