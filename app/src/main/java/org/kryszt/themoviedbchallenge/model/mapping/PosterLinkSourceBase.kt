package org.kryszt.themoviedbchallenge.model.mapping

import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntry

open class PosterLinkSourceBase(
    private val baseUrl: String,
    private val sizeCode: String
) : PosterLinkSource {

    override fun makePosterLink(movie: MovieEntry): String? =
        movie.posterPath?.let { path -> baseUrl + sizeCode + path }

}
