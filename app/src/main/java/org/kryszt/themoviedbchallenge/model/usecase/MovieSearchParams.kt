package org.kryszt.themoviedbchallenge.model.usecase

data class MovieSearchParams(
    val query: String,
    val page: Int = 1
)
