package org.kryszt.themoviedbchallenge.model.usecase

import io.reactivex.Single
import org.kryszt.themoviedbchallenge.model.MovieList
import org.kryszt.themoviedbchallenge.model.mapping.MovieMapper
import org.kryszt.themoviedbchallenge.moviedb.io.MovieDbEndpoint
import org.kryszt.themoviedbchallenge.util.runOnIo

class SearchMoviesUseCase(
    private val searchEndpoint: MovieDbEndpoint,
    private val movieMapper: MovieMapper
) {

    fun findMovies(query: String, page: Int = 1): Single<MovieList> =
        findMovies(MovieSearchParams(query, page))

    fun findMovies(params: MovieSearchParams): Single<MovieList> =
        searchEndpoint
            .searchMovies(params.query, params.page)
            .map(movieMapper::mapMovieEntryList)
            .runOnIo()

}
