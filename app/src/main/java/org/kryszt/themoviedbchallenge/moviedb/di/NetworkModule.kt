package org.kryszt.themoviedbchallenge.moviedb.di

import android.annotation.SuppressLint
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import org.kryszt.themoviedbchallenge.BuildConfig
import org.kryszt.themoviedbchallenge.io.interceptors.AuthInterceptor
import org.kryszt.themoviedbchallenge.io.interceptors.LanguageQueryInterceptor
import org.kryszt.themoviedbchallenge.moviedb.io.MovieDbEndpoint
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

@SuppressLint("ConstantLocale")
val networkModule = module {

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .withDebug()
            .addInterceptor(AuthInterceptor(BuildConfig.THE_MOVIE_DB_API_KEY_V4))
            //This approach requires restarting app to change language, but is simple to configure.
            // And honestly, how often do people change phone's language?
            .addInterceptor(LanguageQueryInterceptor(Locale.getDefault()))
            .build()
    }

    single<Retrofit> {
        @Suppress("RemoveExplicitTypeArguments") //type for 'get' makes it more readable and harder to break by accident
        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(get<OkHttpClient>())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    single<MovieDbEndpoint> {
        get<Retrofit>()
            .create(MovieDbEndpoint::class.java)
    }
}

private fun OkHttpClient.Builder.withDebug(): OkHttpClient.Builder {
    if (!BuildConfig.DEBUG) return this
    return addInterceptor(HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    })
}
