package org.kryszt.themoviedbchallenge.moviedb.di

import android.content.Context
import org.koin.dsl.module
import org.kryszt.themoviedbchallenge.R
import org.kryszt.themoviedbchallenge.model.mapping.MovieListPosterLinkSource
import org.kryszt.themoviedbchallenge.model.mapping.MovieMapper
import org.kryszt.themoviedbchallenge.model.mapping.PosterLinkSource
import org.kryszt.themoviedbchallenge.model.usecase.SearchMoviesUseCase
import org.kryszt.themoviedbchallenge.moviedb.io.MovieDbEndpoint

@Suppress("RemoveExplicitTypeArguments")
val useCasesModule = module {

    factory<PosterLinkSource> {
        val context = get<Context>()
        val imageWidth = context.resources.getDimensionPixelSize(R.dimen.poster_width)
        MovieListPosterLinkSource(
            "https://image.tmdb.org/t/p/",
            imageWidth
        )
    }

    factory<MovieMapper> {
        MovieMapper(
            posterLinkSource = get<PosterLinkSource>()
        )
    }

    factory<SearchMoviesUseCase> {
        SearchMoviesUseCase(get<MovieDbEndpoint>(), get<MovieMapper>())
    }
}
