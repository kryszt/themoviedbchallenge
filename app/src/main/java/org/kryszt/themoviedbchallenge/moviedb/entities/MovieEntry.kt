package org.kryszt.themoviedbchallenge.moviedb.entities

import com.google.gson.annotations.SerializedName

class MovieEntry(
    val id: Int,
    val title: String? = null,
    @SerializedName("overview") val overview: String? = null,
    @SerializedName("poster_path") val posterPath: String? = null,
    @SerializedName("release_date") val releaseDate: String? = null,
)
