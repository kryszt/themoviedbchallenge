package org.kryszt.themoviedbchallenge.moviedb.entities

import com.google.gson.annotations.SerializedName

class MovieEntryList(
    val page: Int,
    val results: List<MovieEntry>,
    @SerializedName("total_pages") val totalPages: Int = 0,
    @SerializedName("total_results") val totalResults: Int
)
