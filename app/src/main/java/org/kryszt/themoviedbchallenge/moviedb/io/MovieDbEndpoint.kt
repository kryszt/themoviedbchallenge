package org.kryszt.themoviedbchallenge.moviedb.io

import io.reactivex.Single
import org.kryszt.themoviedbchallenge.moviedb.entities.MovieEntryList
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieDbEndpoint {
    @GET("search/movie")
    fun searchMovies(@Query("query") query: String, @Query("page") page: Int): Single<MovieEntryList>
}
