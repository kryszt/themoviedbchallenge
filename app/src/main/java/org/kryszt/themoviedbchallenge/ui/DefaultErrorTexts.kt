package org.kryszt.themoviedbchallenge.ui

import android.content.Context
import org.kryszt.themoviedbchallenge.R
import org.kryszt.themoviedbchallenge.model.MovieListError

class DefaultErrorTexts(private val context: Context) : ErrorTexts {
    override fun errorText(error: MovieListError): String = when (error) {
        MovieListError.NO_QUERY -> context.getString(R.string.info_co_search_query)
        MovieListError.CONNECTION_ERROR -> context.getString(R.string.info_connection_error)
    }
}
