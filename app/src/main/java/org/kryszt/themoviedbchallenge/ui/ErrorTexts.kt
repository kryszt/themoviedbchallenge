package org.kryszt.themoviedbchallenge.ui

import org.kryszt.themoviedbchallenge.model.MovieListError

interface ErrorTexts {
    fun errorText(error: MovieListError): String
}
