package org.kryszt.themoviedbchallenge.ui.actions

import android.net.Uri

sealed class MovieListAction {
    data class OpenBrowser(val uri: Uri) : MovieListAction()
}
