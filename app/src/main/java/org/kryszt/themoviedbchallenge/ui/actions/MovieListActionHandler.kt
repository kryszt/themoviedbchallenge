package org.kryszt.themoviedbchallenge.ui.actions

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import org.kryszt.themoviedbchallenge.ui.util.ConsumableItem
import org.kryszt.themoviedbchallenge.ui.util.observeNotNull
import org.kryszt.themoviedbchallenge.util.exhaustive

class MovieListActionHandler(
    private val lifecycleOwner: LifecycleOwner,
    private val context: Context
) {

    fun observe(liveData: LiveData<ConsumableItem<MovieListAction>>) {
        liveData.observeNotNull(lifecycleOwner) { it.consume(this::handleAction) }
    }

    private fun handleAction(action: MovieListAction) {
        when (action) {
            is MovieListAction.OpenBrowser -> openLink(action.uri)
        }.exhaustive()
    }

    private fun openLink(uri: Uri) {
        context.startActivity(Intent(Intent.ACTION_VIEW, uri))
    }
}
