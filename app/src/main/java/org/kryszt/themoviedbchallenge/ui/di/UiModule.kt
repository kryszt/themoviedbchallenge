package org.kryszt.themoviedbchallenge.ui.di

import org.koin.dsl.module
import org.kryszt.themoviedbchallenge.ui.DefaultErrorTexts
import org.kryszt.themoviedbchallenge.ui.ErrorTexts

val uiModule = module {

    single<ErrorTexts> {
        DefaultErrorTexts(get())
    }
}
