package org.kryszt.themoviedbchallenge.ui.list

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import org.kryszt.themoviedbchallenge.R
import org.kryszt.themoviedbchallenge.databinding.ItemLoadingErrorBinding
import org.kryszt.themoviedbchallenge.model.MovieListState
import org.kryszt.themoviedbchallenge.ui.ErrorTexts
import org.kryszt.themoviedbchallenge.ui.util.BasicViewHolder
import org.kryszt.themoviedbchallenge.ui.util.inflate
import org.kryszt.themoviedbchallenge.ui.util.layoutInflater
import org.kryszt.themoviedbchallenge.util.cast

class FooterStateAdapter(private val errorTexts: ErrorTexts) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var footerState: FooterState = FooterState.None
        set(value) {
            if (value != field) {
                field = value
                notifyDataSetChanged()
            }
        }

    fun updateState(movieListState: MovieListState) {
        footerState = movieListState.asFooterState()
    }

    override fun getItemViewType(position: Int): Int =
        footerState.footerViewType.viewType

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            FooterViewType.ERROR.viewType -> ErrorFooterViewHolder(
                ItemLoadingErrorBinding.inflate(parent.layoutInflater(), parent, false)
            )
            else -> BasicViewHolder(parent.inflate(viewType))
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val state = footerState) {
            is FooterState.ErrorFooter -> holder.cast<ErrorFooterViewHolder>().bind(state)
        }
        //no action for other footers
    }

    override fun getItemCount(): Int = if (footerState == FooterState.None) 0 else 1

    private fun MovieListState.asFooterState(): FooterState {
        if (isReadyLoadingMore()) return FooterState.Loading

        return readyError()
            ?.let(errorTexts::errorText)
            ?.let { message -> FooterState.ErrorFooter(message) }
            ?: FooterState.None
    }

    private enum class FooterViewType(@LayoutRes val viewType: Int) {
        NONE(0), LOADING(R.layout.item_loading_more), ERROR(R.layout.item_loading_error)
    }

    private sealed class FooterState(val footerViewType: FooterViewType) {
        object None : FooterState(FooterViewType.NONE)
        object Loading : FooterState(FooterViewType.LOADING)
        class ErrorFooter(val message: String) : FooterState(FooterViewType.ERROR)
    }

    private class ErrorFooterViewHolder(
        private val binding: ItemLoadingErrorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(errorFooter: FooterState.ErrorFooter) {
            binding.errorItemTextView.text = errorFooter.message
        }
    }
}
