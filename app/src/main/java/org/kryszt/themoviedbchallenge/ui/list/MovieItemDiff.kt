package org.kryszt.themoviedbchallenge.ui.list

import androidx.recyclerview.widget.DiffUtil
import org.kryszt.themoviedbchallenge.model.Movie

object MovieItemDiff : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem == newItem
}
