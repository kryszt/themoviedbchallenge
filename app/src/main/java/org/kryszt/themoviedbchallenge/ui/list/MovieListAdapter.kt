package org.kryszt.themoviedbchallenge.ui.list

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import org.kryszt.themoviedbchallenge.databinding.ItemMovieBinding
import org.kryszt.themoviedbchallenge.model.Movie
import org.kryszt.themoviedbchallenge.ui.util.layoutInflater

class MovieListAdapter(
    private val onMovieClick: (Movie) -> Unit,
    private val onIndexBound: (Int) -> Unit
) :
    ListAdapter<Movie, MovieViewHolder>(MovieItemDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder(
            ItemMovieBinding.inflate(parent.layoutInflater(), parent, false),
            onMovieClick
        )

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindMovie(getItem(position))
        onIndexBound(position)
    }
}
