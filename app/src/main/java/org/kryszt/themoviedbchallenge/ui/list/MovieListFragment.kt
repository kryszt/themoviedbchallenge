package org.kryszt.themoviedbchallenge.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.kryszt.themoviedbchallenge.R
import org.kryszt.themoviedbchallenge.databinding.FragmentMovieListBinding
import org.kryszt.themoviedbchallenge.model.MovieListError
import org.kryszt.themoviedbchallenge.model.MovieListState
import org.kryszt.themoviedbchallenge.ui.ErrorTexts
import org.kryszt.themoviedbchallenge.ui.util.*
import org.kryszt.themoviedbchallenge.viewmodel.MovieListViewModel

@KoinApiExtension
class MovieListFragment : Fragment(), KoinComponent {

    private lateinit var binding: FragmentMovieListBinding
    private lateinit var footerStateAdapter: FooterStateAdapter
    private lateinit var movieListAdapter: MovieListAdapter

    private val movieListViewModel: MovieListViewModel by sharedViewModel()
    private val errorTexts: ErrorTexts = get()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentMovieListBinding.inflate(inflater, container, false)
            .also { binding = it }
            .root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initList(binding.movieList)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun initList(movieList: RecyclerView) {
        footerStateAdapter = FooterStateAdapter(errorTexts)
        movieListAdapter = MovieListAdapter(
            onMovieClick = { movieListViewModel.movieClicked(it) },
            onIndexBound = { movieListViewModel.indexShown(it) }
        )
        movieList.adapter = ConcatAdapter(movieListAdapter, footerStateAdapter)
        val itemSeparatorSize =
            requireContext().resources.getDimensionPixelSize(R.dimen.item_separator_height)
        movieList.addHorizontalSeparator(itemSeparatorSize)

        binding.swipeToRefresh.setOnRefreshListener { movieListViewModel.reload() }
    }

    private fun observeViewModel() {
        movieListViewModel.movieListState.observeNotNull(viewLifecycleOwner, this::updateListState)
    }

    private fun updateListState(state: MovieListState) {
        updateMovieItems(state)
        updateLoadingState(state)
        updateEmptyInfo(state)
        updateErrorView(state)
        updateFooter(state)
    }

    private fun updateEmptyInfo(state: MovieListState) {
        binding.noResultsView.setVisibleOrGone(state.isEmptyResult())
    }

    private fun updateMovieItems(state: MovieListState) {
        movieListAdapter.submitList(state.getMovies())
    }

    private fun updateLoadingState(state: MovieListState) {
        if (!state.isLoading()) {
            binding.swipeToRefresh.isRefreshing = false
        }
        binding.loadingIndicator.setVisibleOrGone(state.isClearLoading())
    }

    private fun updateErrorView(state: MovieListState) {
        when (state) {
            is MovieListState.Error -> showError(state.error)
            is MovieListState.Initial -> showError(MovieListError.NO_QUERY)
            else -> hideErrorView()
        }
    }

    private fun showError(error: MovieListError) {
        with(binding.errorView) {
            setVisible()
            text = errorTexts.errorText(error)
        }
    }

    private fun hideErrorView() {
        binding.errorView.setGone()
    }

    private fun updateFooter(state: MovieListState) {
        footerStateAdapter.updateState(state)
    }
}
