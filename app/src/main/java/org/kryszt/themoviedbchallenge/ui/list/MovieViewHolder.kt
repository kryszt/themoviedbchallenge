package org.kryszt.themoviedbchallenge.ui.list

import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.kryszt.themoviedbchallenge.databinding.ItemMovieBinding
import org.kryszt.themoviedbchallenge.model.Movie
import org.kryszt.themoviedbchallenge.ui.util.onClick

class MovieViewHolder(
    private val binding: ItemMovieBinding,
    private val onItemClick: (Movie) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bindMovie(movie: Movie) {
        binding.movieTitle.text = movie.title
        binding.movieSummary.text = movie.summary
        binding.movieReleaseDate.text = movie.formattedReleaseDate

        binding.root.onClick { onItemClick(movie) }

        Picasso
            .get()
            .load(movie.pictureUrl)
            .into(binding.moviePoster)
    }
}
