package org.kryszt.themoviedbchallenge.ui.util

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class BasicViewHolder(view: View) : RecyclerView.ViewHolder(view)
