package org.kryszt.themoviedbchallenge.ui.util

import java.util.concurrent.atomic.AtomicBoolean

/**
 * Allows only single action on item, marking it as consumed after usage.
 *
 */
class ConsumableItem<T>(private val item: T) {

    private val isFresh = AtomicBoolean(true)

    fun consume(action: (T) -> Unit) {
        if (isFresh.getAndSet(false)) {
            action(item)
        }
    }
}

fun <T> T.consumable() = ConsumableItem(this)
