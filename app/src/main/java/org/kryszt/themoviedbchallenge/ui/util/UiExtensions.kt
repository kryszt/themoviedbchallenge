package org.kryszt.themoviedbchallenge.ui.util

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView

fun View.onClick(action: () -> Unit) =
    setOnClickListener { action() }

fun View.layoutInflater(): LayoutInflater =
    LayoutInflater.from(context)

fun ViewGroup.inflate(@LayoutRes layoutId: Int, attachToRoot: Boolean = false): View =
    layoutInflater().inflate(layoutId, this, attachToRoot)

fun RecyclerView.addHorizontalSeparator(sizePx: Int) {
    val space = Rect().apply {
        bottom = sizePx
    }
    addItemDecoration(ItemSpaceDecoration(space))
}

fun <T> LiveData<T>.observeNotNull(lifecycleOwner: LifecycleOwner, action: (T) -> Unit) {
    observe(lifecycleOwner, { item -> item?.also(action) })
}

fun <T : View> T.setVisibleOrGone(visible: Boolean) = if (visible) setVisible() else setGone()

fun <T : View> T.setVisible() = apply {
    visibility = View.VISIBLE
}

fun <T : View> T.setGone() = apply {
    visibility = View.GONE
}

@Deprecated("Search view has only one listener for submit and update query. Using this method will override any other listeners")
fun SearchView.onQuerySubmitted(action: (String) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.also(action)
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }
    })
}
