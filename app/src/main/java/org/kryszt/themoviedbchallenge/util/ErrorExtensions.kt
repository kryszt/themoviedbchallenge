package org.kryszt.themoviedbchallenge.util

fun <T> tryOrNull(action: () -> T): T? =
    try {
        action()
    } catch (e: Exception) {
        null
    }
