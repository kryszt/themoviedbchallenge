package org.kryszt.themoviedbchallenge.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableContainer
import io.reactivex.schedulers.Schedulers

fun <T> Single<T>.runOnIo() = subscribeOn(Schedulers.io())
fun <T> Single<T>.observeOnUi() = observeOn(AndroidSchedulers.mainThread())
fun <T> Observable<T>.observeOnUi() = observeOn(AndroidSchedulers.mainThread())
fun Disposable.addTo(disposeInto: DisposableContainer): Disposable = also {
    disposeInto.add(it)
}

fun <T> Observable<T>.asLiveData(disposeInto: DisposableContainer): LiveData<T> {
    val mutableLiveData = MutableLiveData<T>()
    subscribe(mutableLiveData::postValue)
        .addTo(disposeInto)
    return mutableLiveData
}
