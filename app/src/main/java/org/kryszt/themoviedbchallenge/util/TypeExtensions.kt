package org.kryszt.themoviedbchallenge.util

fun <T> T?.require(): T = requireNotNull(this)

@Suppress("unused")
fun Any?.exhaustive() = Unit

inline fun <reified T> Any?.cast(): T = this as T
inline fun <reified T> Any?.castSafe(): T? = this as? T

inline fun <reified T> Any?.isOf(): Boolean = this is T

fun Boolean?.orElseFalse() = this ?: false
fun Boolean?.orElseTrue() = this ?: true

fun <T> T?.orElse(default: T) = this ?: default
