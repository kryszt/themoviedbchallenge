package org.kryszt.themoviedbchallenge.viewmodel

class InfiniteScrollSupport(
    private val preloadThreshold: Int,
    private val loadedCount: () -> Int,
    private val canLoadMore: () -> Boolean,
    private val requestMore: () -> Unit
) {

    fun indexShown(index: Int) {
        if (canLoadMore() && atLoadMoreThreshold(index)) {
            requestMore()
        }
    }

    private fun atLoadMoreThreshold(index: Int): Boolean =
        index + preloadThreshold > loadedCount()
}
