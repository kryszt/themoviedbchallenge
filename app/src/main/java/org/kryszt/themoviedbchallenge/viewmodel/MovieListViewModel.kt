package org.kryszt.themoviedbchallenge.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.kryszt.themoviedbchallenge.model.Movie
import org.kryszt.themoviedbchallenge.model.MovieListState
import org.kryszt.themoviedbchallenge.model.MovieListStateHolder
import org.kryszt.themoviedbchallenge.ui.actions.MovieListAction
import org.kryszt.themoviedbchallenge.ui.util.ConsumableItem
import org.kryszt.themoviedbchallenge.ui.util.consumable
import org.kryszt.themoviedbchallenge.util.DisposableViewModel
import org.kryszt.themoviedbchallenge.util.asLiveData
import org.kryszt.themoviedbchallenge.util.require

class MovieListViewModel(
    private val movieListStateHolder: MovieListStateHolder
) : DisposableViewModel() {

    private val infiniteScrollSupport = InfiniteScrollSupport(
        preloadThreshold = PRELOAD_THRESHOLD,
        loadedCount = { currentState.getMoviesCount() },
        canLoadMore = { currentState.canLoadMore() },
        requestMore = { movieListStateHolder.loadMore() }
    )

    private val currentState: MovieListState
        get() = movieListStateHolder.currentState

    private val _navigationEvent = MutableLiveData<ConsumableItem<MovieListAction>>()
    val navigationEvent: LiveData<ConsumableItem<MovieListAction>> = _navigationEvent

    val movieListState: LiveData<MovieListState> =
        movieListStateHolder.observableState.asLiveData(disposables)


    fun movieClicked(movie: Movie) {
        _navigationEvent.postValue(
            MovieListAction.OpenBrowser(Uri.parse(movie.detailsLink)).consumable()
        )
    }

    fun querySubmitted(newQuery: String) {
        movieListStateHolder.loadList(newQuery)
    }

    fun indexShown(index: Int) {
        infiniteScrollSupport.indexShown(index)
    }

    fun reload() {
        movieListStateHolder.reload()
    }

    companion object {
        private const val PRELOAD_THRESHOLD = 5
    }
}
