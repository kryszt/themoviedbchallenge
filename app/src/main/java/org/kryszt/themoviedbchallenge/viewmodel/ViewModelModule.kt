package org.kryszt.themoviedbchallenge.viewmodel

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.kryszt.themoviedbchallenge.model.MovieListStateHolder

@Suppress("RemoveExplicitTypeArguments")
val viewModelModule = module {
    viewModel<MovieListViewModel> {
        MovieListViewModel(get<MovieListStateHolder>())
    }
}
