package org.kryszt.themoviedbchallenge.model.mapping

import org.junit.Assert
import org.junit.Test
import java.util.*

class MovieDbReleaseDateFormatMapperTest {

    private val sut = MovieDbReleaseDateFormatMapper(Locale.US)

    @Test
    fun `should format correct dates`() {
        Assert.assertEquals("Nov 30, 1982", sut.mapDate("1982-11-30"))
        Assert.assertEquals("Jan 25, 1985", sut.mapDate("1985-01-25"))
        Assert.assertEquals("Jan 1, 1970", sut.mapDate("1970-01-01"))
        Assert.assertEquals("Dec 12, 2121", sut.mapDate("2121-12-12"))
    }

    @Test
    fun `should return null for malformed dates`() {
        Assert.assertNull(sut.mapDate("2121.12.12"))
        Assert.assertNull(sut.mapDate("not a date"))
        //TODO - by some strange logic those malformed dates are parsed anyway by default java classes
//        Assert.assertNull(sut.mapDate("2012-12-1"))
//        Assert.assertNull(sut.mapDate("2012-1-11"))
//        Assert.assertNull(sut.mapDate("11-11-2011"))
    }

}
