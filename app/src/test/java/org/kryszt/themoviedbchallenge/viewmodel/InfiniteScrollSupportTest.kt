package org.kryszt.themoviedbchallenge.viewmodel

import org.junit.Assert
import org.junit.Test

class InfiniteScrollSupportTest {

    @Test
    fun `do not call callback when index is below threshold`() {
        indexesBeforeThreshold.forEach { index ->
            expectNoCall(true, index)
        }
    }

    @Test
    fun `call load more when index at threshold`() {
        indexesAtThreshold.forEach { index ->
            expectLoadMoreCall(index)
        }
    }

    @Test
    fun `do not call callback when cannot load more`() {
        allIndexes.forEach { index ->
            expectNoCall(false, index)
        }
    }

    private fun expectLoadMoreCall(index: Int) {
        val spy = CallbackSpy()
        makeTestObject(true, spy).indexShown(index)
        Assert.assertTrue(
            "Callback not called for index $index with canLoadMore = true",
            spy.wasCalled()
        )
    }

    private fun expectNoCall(canLoadMore: Boolean, index: Int) {
        val spy = CallbackSpy()
        makeTestObject(canLoadMore, spy).indexShown(index)
        Assert.assertFalse(
            "Callback called but not expected for index $index with canLoadMore = $canLoadMore",
            spy.wasCalled()
        )
    }


    private class CallbackSpy {

        private var called: Boolean = false

        fun callback() {
            called = true
        }

        fun wasCalled() = called
    }

    companion object {

        private const val THRESHOLD = 5
        private const val SIZE = 10
        private val indexesBeforeThreshold = 0..5
        private val indexesAtThreshold = 6..10
        private val allIndexes = 0..10

        private fun makeTestObject(
            canLoadMore: Boolean,
            callback: CallbackSpy
        ): InfiniteScrollSupport {
            return InfiniteScrollSupport(
                preloadThreshold = THRESHOLD,
                loadedCount = { SIZE },
                canLoadMore = { canLoadMore },
                requestMore = callback::callback
            )
        }
    }

}
